<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Category;

    /**
     * @ORM\ManyToOne(targetEntity=Material::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Material;

    /**
     * @ORM\ManyToOne(targetEntity=Age::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Age;

    /**
     * @ORM\Column(type="text")
     */
    private $AgeText;

    /**
     * @ORM\Column(type="text")
     */
    private $NumberPlayers;

    /**
     * @ORM\Column(type="text")
     */
    private $Duration;

    /**
     * @ORM\Column(type="text")
     */
    private $Competency;

    /**
     * @ORM\Column(type="text")
     */
    private $Thematic;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $barcode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->Category;
    }

    public function setCategory(?Category $Category): self
    {
        $this->Category = $Category;

        return $this;
    }

    public function getMaterial(): ?Material
    {
        return $this->Material;
    }

    public function setMaterial(?Material $Material): self
    {
        $this->Material = $Material;

        return $this;
    }

    public function getAge(): ?Age
    {
        return $this->Age;
    }

    public function setAge(?Age $Age): self
    {
        $this->Age = $Age;

        return $this;
    }

    public function getAgeText(): ?string
    {
        return $this->AgeText;
    }

    public function setAgeText(string $AgeText): self
    {
        $this->AgeText = $AgeText;

        return $this;
    }

    public function getNumberPlayers(): ?string
    {
        return $this->NumberPlayers;
    }

    public function setNumberPlayers(string $NumberPlayers): self
    {
        $this->NumberPlayers = $NumberPlayers;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->Duration;
    }

    public function setDuration(string $Duration): self
    {
        $this->Duration = $Duration;

        return $this;
    }

    public function getCompetency(): ?string
    {
        return $this->Competency;
    }

    public function setCompetency(string $Competency): self
    {
        $this->Competency = $Competency;

        return $this;
    }

    public function getThematic(): ?string
    {
        return $this->Thematic;
    }

    public function setThematic(string $Thematic): self
    {
        $this->Thematic = $Thematic;

        return $this;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }
}
