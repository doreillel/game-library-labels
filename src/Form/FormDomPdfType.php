<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Repository\GameRepository;

class FormDomPdfType extends AbstractType
{
    public GameRepository $gameRepo;

    function __construct(GameRepository $gameRepo)
    {
        $this->gameRepo = $gameRepo;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $first_available_id = $this->gameRepo->getFirstAvailableId();

        $last_available_id = $this->gameRepo->getLastAvailableId();

        $values = [];
        for ($i = $first_available_id; $i <= $last_available_id; $i++) {
            $values[$i] = $i;
        }
        
        $builder
            ->add('first_id', ChoiceType::class, [
                'placeholder' => false,
                'label' => 'First id: ',
                'choices' => $values
            ])
            ->add('last_id', ChoiceType::class, [
                'placeholder' => false,
                'label' => 'Last id: ',
                'choices' => $values
            ])
            ->add('Send', SubmitType::class, [
                'attr' => ['class' => 'save'],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
