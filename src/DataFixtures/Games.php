<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Category;
use App\Entity\Age;
use App\Entity\Game;
use App\Entity\Material;


class Games extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $category = new Category();
        $category->setName("Category Test");
        $category->setLogo("Cards.png");
        $manager->persist($category);
        $manager->flush();

        $material = new Material();
        $material->setTitle("Material Test");
        $material->setLogo("cards.png");
        $manager->persist($material);

        $age = new Age();
        $age->setTitle("Age Test");
        $age->setColor("#FF0000");
        $age->setComment("Age de Test");
        $manager->persist($age);

        
        $game = new Game();
        $game->setTitle("Jeu Test");
        $game->setCategory($category);
        $game->setMaterial($material);
        $game->setAge($age);
        $game->setAgeText("over 10 years");
        $game->setNumberPlayers("2 to 4");
        $game->setDuration("10 mns");
        $game->setCompetency("Competency");
        $game->setBarcode("Welcome");
        $game->setThematic("Thematic");
        $manager->persist($game);
        

        $manager->flush();
    }
}
