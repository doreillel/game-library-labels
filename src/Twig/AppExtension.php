<?php

// src/Twig/AppExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Picqer\Barcode\BarcodeGeneratorPNG;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('barcode', [$this, 'barCode']),
        ];
    }

    public function barCode(string $value): string
    {
        $generator = new BarcodeGeneratorPNG();
        $barcode =  '<img class="barcode" src="data:image/png;base64,' . base64_encode($generator->getBarcode($value, $generator::TYPE_CODE_128)) . '">';

        return $barcode;
    }
}
