<?php

namespace App\Controller\Admin;

use Dompdf\Dompdf;
use App\Entity\Game;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GameCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Game::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('Title'),
            TextField::new('Competency'),
            TextField::new('Thematic'),
            AssociationField::new('Category')->setFormTypeOptions([
                'by_reference' => true,
            ])/*->autocomplete()*/,
            AssociationField::new('Material')->setFormTypeOptions([
                'by_reference' => true,
            ])/*->autocomplete()*/,
            AssociationField::new('Age')->setFormTypeOptions([
                'by_reference' => true,
            ])/*->autocomplete()*/,
            TextField::new('AgeText'),
            TextField::new('NumberPlayers'),
            TextField::new('Duration'),
            TextField::new('Barcode')
            ->setFormTypeOptions([
                'attr' => [
                    'maxlength' => 12
                ]
            ])
            ,
        ];
    }

    public function configureActions(Actions $actions): Actions
    {

        $doClone = Action::new('doClone', 'Clone')
            ->linkToCrudAction('doClone');

        return $actions
            ->addBatchAction(Action::new('show tickets', 'Show tickets')
                ->linkToCrudAction('showTickets')
                ->addCssClass('btn btn-primary'))
            ->add(Crud::PAGE_INDEX, $doClone)
            ->add(Crud::PAGE_EDIT, $doClone)
            ->add(Crud::PAGE_NEW, ACTION::SAVE_AND_CONTINUE)
        ;
    }


    public function showTickets(BatchActionDto $batchActionDto): Response
    {
        $className = $batchActionDto->getEntityFqcn();
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        
        $games = [];
        foreach ($batchActionDto->getEntityIds() as $id) {
            $games[] = $entityManager->find($className, $id);
        }


        $content = $this->render('admin/pdf.html.twig', [
            'games' => $games,
        ]);

        $dompdf = new Dompdf([
            'enable_remote' => true,
        ]);
        $dompdf->loadHtml($content->getContent());
        $dompdf->render();

        return new Response($dompdf->output(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; attachment; filename=etiquettes',
        ]);

        //return $this->redirect($batchActionDto->getReferrerUrl());
    }

    public function doClone(AdminContext $context, EntityManagerInterface $em, AdminUrlGenerator $adminUrlGenerator): Response
    {
        $gameToClone = $context->getEntity()->getInstance();
        $clone = clone $gameToClone;
        $em->persist($clone);
        $em->flush();
        
        $id = $clone->getId();

        $url = $adminUrlGenerator
        ->setController(self::class)
        ->setAction(Crud::PAGE_EDIT)
        ->setEntityId($id)
        ->generateUrl();

        return $this->redirect($url);
    }
}
