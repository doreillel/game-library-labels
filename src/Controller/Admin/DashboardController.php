<?php

namespace App\Controller\Admin;

use App\Entity\Age;
use App\Entity\Game;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Material;
use App\Form\FormDomPdfType;
use App\Repository\GameRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Dompdf\Dompdf;

class DashboardController extends AbstractDashboardController
{
    protected Request $request;

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('admin_form');
    }

    /**
     * @Route("/admin/form", name="admin_form")
     */
    public function form(Request $request, GameRepository $gr): Response
    {
        $form_creator = $this->createForm(FormDomPdfType::class);
        
        $form_creator->handleRequest($request);
        if ($form_creator->isSubmitted() && $form_creator->isValid()) {
            $first_id = $form_creator->get('first_id')->getData();
            $last_id = $form_creator->get('last_id')->getData();
            
            if ($first_id > $last_id) {
                $errors = "Last Id must be equal or higher than First Id";
                $form_creator->addError(new FormError($errors));
            } else {
                $games = $gr->getListFromFirstIdToLastId($first_id, $last_id);

                $content = $this->render('admin/pdf.html.twig', [
                    'games' => $games,
                ]);

                $dompdf = new Dompdf([
                    'enable_remote' => true,
                ]);
                $dompdf->loadHtml($content->getContent());
                $dompdf->render();
        
                return new Response($dompdf->output(), 200, [
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'inline; attachment; filename=etiquettes',
                ]);
            }
        }

        $form = $form_creator->createView();
        return $this->render('admin/dashboard.html.twig', [
            'form' => $form,
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Backoffice');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Dashboard', 'fa fa-home'),
            MenuItem::section('Utilisateurs'),
            MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class),
            MenuItem::section('Categories'),
            MenuItem::linkToCrud('Categories', 'fas fa-list', Category::class),
            MenuItem::section('Matériel'),
            MenuItem::linkToCrud('Matérial', 'fas fa-list', Material::class),
            MenuItem::section('Tranche d\'age'),
            MenuItem::linkToCrud('Age', 'fas fa-child', Age::class),
            MenuItem::section('Jeux'),
            MenuItem::linkToCrud('Jeux', 'fas fa-dice', Game::class)
        ];
    }
}
