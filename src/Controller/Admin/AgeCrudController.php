<?php

namespace App\Controller\Admin;

use App\Entity\Age;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;

class AgeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Age::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            ColorField::new('color')->showSample(),
            TextField::new('comment'),
        ];
    }
}
