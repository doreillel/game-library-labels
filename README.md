## Project Description

The goal of this project is to create labels for categorizing board games, whether for personal or professional use (e.g., game library).

Each label includes the following information:

-   A primary title
-   A subtitle
-   Required age
-   Number of players
-   Playing time
-   A color for easy categorization
-   Two logos to distinguish games on a shelf
-   A barcode for linking to third-party software (for professional use)

![Example of label](label.png)

The project is built on Symfony 5.4 and is compatible with PHP 7.4 and higher.

### Local Installation

To install it locally, it is recommended to use `ddev` and follow these commands:

1.  Rename the "ddev" folder to ".ddev".
2.  Run the command `ddev composer install`.
3.  Run `ddev npm install`.
4.  Run `ddev yarn build`.
5.  Run `ddev exec 'bin/console doctrine:migrations:migrate'`.
6.  Finally, run `ddev exec 'bin/console doctrine:fixtures:load'`.
